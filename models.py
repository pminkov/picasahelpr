from google.appengine.ext import db



class LoggedInImpression(db.Model):
  user = db.UserProperty()
  date = db.DateTimeProperty(auto_now_add=True)
