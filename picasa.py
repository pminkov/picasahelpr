import logging
import gdata.photos.service


PICASA_SCOPE = 'http://picasaweb.google.com/data/'


class PicasaClient(gdata.photos.service.PhotosService):
  def __init__(self, td):
    gdata.photos.service.PhotosService.__init__(self)
    self.td = td
    gdata.alt.appengine.run_on_appengine(self)
    pass

  def TryToFindTokenAndSetCurrentToken(self, request_uri=None):
    stored_token = self.token_store.find_token(PICASA_SCOPE)
    logging.info(stored_token)
    try:
      # The token might have been revoked and we want to know
      # that before we start making requests.
      self.AuthSubTokenInfo()
    except gdata.service.RequestError:
      stored_token = None
    except gdata.service.NonAuthSubToken:
      stored_token = None

    if stored_token and stored_token.valid_for_scope(PICASA_SCOPE):
      logging.info('Found stored token!')
      logging.info(stored_token)
      logging.info(stored_token.__class__)
      self.current_token = stored_token

    elif request_uri:
      auth_token = gdata.auth.extract_auth_sub_token_from_url(request_uri)
      session_token = None

      if auth_token:
        try:
          session_token = self.upgrade_to_session_token(auth_token)
	except gdata.service.TokenUpgradeFailed:
	  # Repro:
	  #  1. Grant access. Url has auth_sub_scopes and token url params.
	  #  2. Revoke access in separate window.
	  #  3. Reload url from 1. Token can't be upgraded!
	  pass

        if session_token:
          self.token_store.add_token(session_token)
          self.current_token = session_token

    if self.current_token: 
      return True

    return False


  def GetAlbumData(self, uri):
    photos = self.GetFeed(uri).entry

    photos_by_size = []
    total = 0
    for i in range(0, len(photos)):
      photo = photos[i]
      size = int(photo.size.text)
      photos_by_size += [(size, i)]
      total += size

    self.td['photos_avg'] = '%.1f' % ((total / len(photos)) / (1024.))

    photos_by_size.sort(reverse=True)

    photos_list = []
    for i in range(0, 5):
      index = photos_by_size[i][1]
      photo = photos[index]

      entry = {}
      entry['size_kb'] = '%.1f' % (int(photo.size.text) / 1024.)
      entry['thumbnail_url'] = photo.media.thumbnail[1].url
      photos_list += [entry]

    self.td['photos_list'] = photos_list


  def _GetSortedAlbumIndex(self, albums):
    all = []
    for i in range(0, len(albums)):
      bytes = int(albums[i].bytesUsed.text)
      mb = bytes / (1024. * 1024.)
      all += [(mb, i)]

    all.sort(reverse=True)

    return all


  def _FillQuotaDetails(self, feed):
    quotalimit = int(feed.quotalimit.text)
    quotacurrent = int(feed.quotacurrent.text)

    self.td['quotalimit'] = '%.1f' % (quotalimit / (1024. * 1024.))
    self.td['quotacurrent'] = '%.1f' % (quotacurrent / (1024. * 1024.))
    self.td['quotapercent'] = '%.2f' % (float(quotacurrent) / float(quotalimit)
                                        * 100)

  def _GetAvgPhotoSize(self, album):
    numphotos = album.numphotos.text
    ret = ''
    if int(numphotos) > 0:
      ret = '%.1f' % ((float(album.bytesUsed.text) / float(numphotos)) / 1024.)
    else:
      ret = '0.0'
    return ret


  def _FillPerAlbumData(self, feed):
    albums = feed.entry
    all = self._GetSortedAlbumIndex(albums)
    self.td['len_albums'] = len(all)

    albums_template = []
    for i in range(0, len(all)):
      size_mb = all[i][0]
      index = all[i][1]
      album = albums[ index ]

      entry = {}
      entry['size_mb'] = '%.1f' % size_mb
      entry['name'] = album.title.text
      numphotos = int(album.numphotos.text)
      entry['numphotos'] = numphotos
      entry['avg_photo_size'] = self._GetAvgPhotoSize(album)
      if album.access.text == 'private':
        entry['isprivate'] = 1
      entry['photos_uri'] = album.GetPhotosUri()
      entry['album_index'] = i

      albums_template += [entry]

    self.td['albums'] = albums_template


  def FillDictionaryWithPicasaData(self):
    feed = self.GetUserFeed()
    self._FillQuotaDetails(feed)
    self._FillPerAlbumData(feed)

    #logging.info(feed.quotalimit.__class__)

    #albums = feed.entry
    #for i in range(0, len(albums)):
    #  self._GetAlbumData(albums[i])

    #if len(all):
    #  self._GetAlbumData(all[0][2])
