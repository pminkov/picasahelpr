google.load("jquery", "1.5.1")

google.setOnLoadCallback(function() {
  $('.albumname').css('background-color', '#eee')
  $('.albumname').mouseover(function () {
    $(this).css('background-color', '#0099ff');
  }).mouseout(function() {
    $(this).css('background-color', '#eee');
  });

  $('.albumcardrow').hide();

  $('.albumname').click(function() {
    var photosUri = $(this).attr('photos_uri');
    var albumIndex = $(this).attr('album_index');

    var clickedOn = this;
    var row = $('#albumcardrow-' + albumIndex);
    row.toggle();

    jQuery.get('/album', {'photos_uri':photosUri}, function(data) {
      $('#cardelement-' + albumIndex).html(data);
    });
  });
});

