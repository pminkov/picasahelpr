from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext import db
from google.appengine.api import users
import os
import gdata.photos.service
import gdata.service
import gdata.alt.appengine
import atom.url
import logging
import picasa
import hostname
from handlers import album
import models


class MainPage(webapp.RequestHandler):

  def get(self):
    self.td = {}

    user = users.get_current_user()

    if user:
      self.HandleLoggedIn(user)
    else:
      self.HandleLoggedOut()

    self.WriteTemplate()

  def HandleLoggedIn(self, user):
    self.td['nickname'] = user.nickname()
    self.td['signout_url'] = users.create_logout_url(self.request.uri)

    impression = models.LoggedInImpression()
    impression.user = user
    impression.put()

    pws = picasa.PicasaClient(self.td)
    authorized = pws.TryToFindTokenAndSetCurrentToken(self.request.uri)

    if authorized:
      pws.FillDictionaryWithPicasaData()

    else:
      self.ProduceGrantAccessUrl(pws)


  def ProduceGrantAccessUrl(self, pws):
    # TODO(petko): This is repeated in picasa.py. fix this.
    PICASA_SCOPE = 'http://picasaweb.google.com/data/'
    
    # No token, user has to grant access.
    next_url = atom.url.Url('http', hostname.HOST_NAME, path='/')
    auth_sub_url = pws.GenerateAuthSubURL(next_url,
      (PICASA_SCOPE,), secure=False, session=True)
    self.td['grant_access_url'] = auth_sub_url


  def HandleLoggedOut(self):
    self.td['signin_url'] = users.create_login_url(self.request.uri)


  def WriteTemplate(self):
    path = os.path.join(os.path.dirname(__file__), '../templates/index.html')
    self.response.out.write(template.render(path, self.td))
