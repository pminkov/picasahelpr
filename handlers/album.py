from google.appengine.ext import webapp
import gdata.alt.appengine
import picasa
import logging
import urllib
import os
from google.appengine.ext.webapp import template


class AlbumHandler(webapp.RequestHandler):
  def get(self):
    td = {}

    photos_uri = self.request.get('photos_uri')
    photos_url = urllib.unquote(photos_uri)

    picasa_client = picasa.PicasaClient(td)
    authorized = picasa_client.TryToFindTokenAndSetCurrentToken()
    if authorized:
      picasa_client.GetAlbumData(photos_uri)
    else:
      logging.error('no token found')

    path = os.path.join(os.path.dirname(__file__), '../templates/albumcard.html')
    self.response.out.write(template.render(path, td))

      
