from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app
import os
import gdata.alt.appengine
import logging
from handlers import album
from handlers import main

class AboutPage(webapp.RequestHandler):
  def get(self):
    path = os.path.join(os.path.dirname(__file__), 'templates/about.html')
    logging.info('\033[95m HELLO \033[0m')
    self.response.out.write(template.render(path, {}))


class FeedbackPage(webapp.RequestHandler):
  def get(self):
    path = os.path.join(os.path.dirname(__file__), 'templates/feedback.html')
    self.response.out.write(template.render(path, {}))


application = webapp.WSGIApplication(
  [('/', main.MainPage),
   ('/about', AboutPage),
   ('/feedback', FeedbackPage),
   ('/album', album.AlbumHandler)
   ],
  debug=True)


def main():
  run_wsgi_app(application)

if __name__ == '__main__':
  main()
